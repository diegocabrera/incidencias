<?php

namespace App\Modules\Base\Models;

use App\Modules\Base\Models\Modelo;
use Carbon\Carbon;
use App\Modules\Base\Models\Mensaje;
use App\Modules\Base\Models\TipoNotificacion;

class Notificaciones extends Modelo
{
    protected $table = 'notificaciones';
    protected $fillable = [
        "usuario_id",
        "enviado_id",
        "mensaje_id",
        "operacion_id",
        "visto",
        "tipo_notificacion_id"
    ];

    public function usuario()
    {
        //return Usuario::find(Estructura::find($this->usuario_id)->usuario_id);
    }
    public function enviado()
    {
        return Usuario::find($this->enviado_id)->personas->nombres;
    }
    public function mensaje()
    {
        return $this->belongsTo('App\Modules\Base\Models\Mensaje', 'mensaje_id');
    }
    public function TipoNotificacion()
    {
        return $this->belongsTo('App\Modules\Base\Models\TipoNotificacion', 'tipo_notificacion_id');
    }

    public function getCreatedAtAttribute($value){
        return Carbon::parse($value)->format('d/m/Y H:m:s');
    }

}
