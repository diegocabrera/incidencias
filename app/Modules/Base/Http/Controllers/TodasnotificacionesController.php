<?php

namespace App\Modules\Base\Http\Controllers;

//Controlador Padre
use App\Modules\Base\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use App\Modules\Base\Http\Requests\TodasnotificacionesRequest;

//Modelos
use App\Modules\Base\Models\Notificaciones;

class TodasnotificacionesController extends Controller
{
    protected $titulo = 'Todasnotificaciones';

    public $js = [
        'Notificaciones'
    ];
    
    public $css = [
        'Notificaciones'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        $Todasnotificaciones = Notificaciones::select ( "usuario_id","enviado_id","mensaje_id","operacion_id","visto","tipo_notificacion_id")
                   ->whereNull('Notificaciones.deleted_at')
                    ->orderBy('Notificaciones.id', 'desc')
                   ->paginate(10);
            

            return $this->view('base::Todasnotificaciones', [
            'Todasnotificaciones'  => $Todasnotificaciones
            
         ]);




    }

   
   /*  public function datatable(Request $request)
    {
        $sql = Notificaciones::select([
            'id', 'usuario_id', 'enviado_id', 'mensaje_id', 'operacion_id', 'visto', 'tipo_notificacion_id', 'nuevo', 'nuevo2', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }*/
}