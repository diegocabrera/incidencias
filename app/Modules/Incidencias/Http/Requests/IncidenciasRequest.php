<?php

namespace App\Modules\Incidencias\Http\Requests;

use App\Http\Requests\Request;

class IncidenciasRequest extends Request {
    protected $reglasArr = [
		'titulo' => ['required', 'min:3', 'max:255'], 
		'descripcion' => ['required'], 
		'app_usuario_id' => ['required', 'integer'], 
		'app_perfil_id' => ['required', 'integer'], 
		'estatus_id' => ['required', 'integer'], 
		'correo' => ['required', 'min:3', 'max:255'], 
		'nuevo' => ['required']
	];
}