<?php

namespace App\Modules\Incidencias\Models;

use App\Modules\Base\Models\Modelo;
use Carbon\Carbon;

use App\Modules\Base\Models\Usuario;

class NuevaIncidencia extends Modelo
{
    protected $table = 'nuevaincidencia';
   
    protected $fillable = ["titulo","descripcion","Departameto","Direccion","Numero"];
    protected $campos = [
    'titulo' => [
        'type' => 'text',
        'label' => 'Titulo',
        'placeholder' => 'Titulo de la Incidencia'
    ],
    'descripcion' => [
        'type' => 'textarea',
        'label' => 'Descripcion',
        'placeholder' => 'Descripcion del Incidencias'
    ],
    'Departameto' => [
        'type' => 'textarea',
        'label' => 'Departameto',
        'placeholder' => 'Indique el Departameto',
        
    ],
    'Direccion' => [
        'type' => 'textarea',
        'label' => 'Direccion',
        'placeholder' => 'Indique la Direccion exacta',
        
    ],
    'Numero' => [
        'type' => 'textarea',
        'label' => 'Numero',
        'placeholder' => 'coloque el numero telefonico',
        
    ]
    
];


    public function setPublishedAtAttribute($value){
        $this->attributes['published_at'] = Carbon::createFromFormat('d/m/Y H:i', $value);
    }
    // public function __construct(array $attributes = array())
    // {
    //     parent::__construct($attributes);
    //     $this->campos['categorias_id']['option'] = categorias::pluck('nombre', 'id');
    // }

   

}
