var aplicacion, $form, tabla, $archivo_actual = '', $archivos = {}, cordenadasImagen;
$(function() {
	aplicacion = new app('formulario', {
		'antes' : function(accion){
			$("#archivos").val(jsonToString($archivos));
			//$("#contenido_html").val(CKEDITOR.instances.contenido.getData());
		},
		'limpiar' : function(){
			tabla.ajar.reload();
			$archivos = {};

			$("table tbody tr", "#fileupload","#Departameto","#titulo","#contenido_html","#Descripcion","#Numero","#Direccion").remove();

		},

	    'buscar' : function(r){
			//CKEDITOR.instances.contenido.setData(r.contenido_html);
			//$('#btn1').click();

			$("table tbody", "#fileupload").html(tmpl("template-download", r));
			$("table tbody .fade", "#fileupload").addClass('in');

			var archivos = r.files;
			$archivos = {};
			for(var i in archivos){
				$archivos[archivos[i].id] = archivos[i].data;
			}
		}
	});

	$form = aplicacion.form;


	$('#eliminar').attr("disabled", true);

    tabla = datatable('#tabla', {
        ajax: $url + "datatable",
        columns: [
            { data: 'id',  name: 'id' },
            { data: 'titulo', name: 'titulo'},
            { data: 'contenido_html', name: 'contenido_html'},
            { data: 'Numero', name: 'Numero'},
            { data: 'Direccion', name: 'Direccion'},

        ]
    });

    $('#tabla').on("click", "tbody tr", function(){
        aplicacion.buscar(this.id);
    });

		$('#published_at', $form).datetimepicker();
		//var contenido = CKEDITOR.replace('contenido');
        $('#fileupload').fileupload({
        url: $url + 'subir',
        disableImageResize: /Android(?!.*Chrome)|Opera/.test(window.navigator.userAgent),
        maxFileSize: 999000,
        //formData: {example: 'test'},
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i
    }).bind('fileuploaddone', function (e, data) {
        var archivo = data.result.files[0];
        $archivos[archivo.id] = archivo.data;
    });
    $('#fileupload').on('click', '.btn-danger', function(evn){
        evn.preventDefault();
        $archivo_actual = $(this).parents('tr').data('id');
		
 });

});

	$(document).ready(function () {
    	$("#titulo").keyup(function () {
	        var value = slug($("#titulo").val());
	        $("#slug").val(value);
    	});
	});
    function dataImagen(cordenadas){
    cordenadasImagen = cordenadas;
    }

    function stringToJson(str){
    return $.parseJSON(str);
    }

    function jsonToString(json){
    return JSON.stringify(json);
    }
